# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, fetchFromGitHub, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./code.nix
    ./game.nix
    ./terminal.nix
    ./pkgs.nix
#    ./unstable.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      # assuming /boot is the mount point of the  EFI partition in NixOS (as the installation section recommends).
      efiSysMountPoint = "/boot";
    };
    grub = {
      # despite what the configuration.nix manpage seems to indicate,
      # as of release 17.09, setting device to "nodev" will still call
      # `grub-install` if efiSupport is true
      # (the devices list is not used by the EFI grub install,
      # but must be set to some value in order to pass an assert in grub.nix)
      devices = [ "nodev" ];
      efiSupport = true;
      enable = true;
      version = 2;
      gfxpayloadEfi = "keep";
      gfxmodeEfi = "1280x1024";
    };
  };

  hardware.cpu.intel.updateMicrocode = true;
  hardware.openrazer.enable = true;

  hardware.opengl = {
    driSupport = true;
    driSupport32Bit = true;
  };

  boot.supportedFilesystems = [ "zfs" ];

  networking.hostId = "331987bd";
  networking.hostName = "will-nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = true;
  # networking.interfaces.enp4s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  console = {
    earlySetup = true;
    useXkbConfig = true;
    font = "Lat2-Terminus16";
  #   keyMap = "us";
  };

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;

    fontconfig = {
      useEmbeddedBitmaps = true;

      # ultimate = {
      #   enable = true;
      #   preset = "osx";
      # };

      defaultFonts = {
        serif = [ "Source Serif Pro" "DejaVu Serif" ];
        sansSerif = [ "Source Sans Pro" "DejaVu Sans" ];
        monospace = [ "Fira Code" "Hasklig" ];
      };
    };

    fonts = with pkgs; [
      hasklig
      source-code-pro
      overpass
      nerdfonts
      fira
      fira-code
      fira-mono
    ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  nixpkgs.config.firefox.enablePlasmaBrowserIntegration = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  #   pinentryFlavor = "gnome3";
  # };
  programs = {
    adb.enable = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;

  services.zfs.autoScrub.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip pkgs.gutenprint];

  services.avahi.enable = true;
  services.avahi.nssmdns = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    xkbOptions = "caps:none,compose:ralt";
    autoRepeatDelay = 200;
    autoRepeatInterval = 25;
    videoDrivers = [ "nvidia" ];
    displayManager.sddm = {
      enable = true;
      autoNumlock = true;
      # theme = "sugar-dark";
    };
    desktopManager.plasma5.enable = true;
    # windowManager.dwm.enable = true;
    windowManager.bspwm.enable = true;
    # windowManager.bspwm.configFile = /etc/nixos/bspwmrc;
    # windowManager.bspwm.sxhkd.configFile = /etc/nixos/sxhkdrc;
    # displayManager.session = [
    #   # {
    #   #   manage = "custom session";
    #   #   name = "custom xsession";
    #   #   start = ''exec /usr/bin/env KDEWM=dwm /nix/store/19dgn4kqfczfzqx9n9ixrz3nxpl76kfv-xsession'';
    #   # }
    #   { manage = "desktop";
    # name = "Plasma_dwm";
    # start = ''
    #   exec /usr/bin/env KDEWM=dwm /nix/store/p7wkp05243157rn62y43x1y559c32bjc-system-path/bin/startplasma-x11
    # '';
  # }
    #   { manage = "desktop";
    # name = "Plasma_bspwm";
    # start = ''
    #   exec /usr/bin/env KDEWM=bspwm /nix/store/p7wkp05243157rn62y43x1y559c32bjc-system-path/bin/startplasma-x11
    # '';
  # }
    # ];
  };

  # nixpkgs.config.packageOverrides = pkgs: {
  #   dwm = pkgs.dwm.override {
  #     patches = [
  #       # ./dwm/dwm-uselessgap-6.2.diff
  #       # ./dwm/dwm-flextile-5.8.2.diff
  #       ./dwm/dwm-pertag-flextile_deluxe-6.2.diff
  #       ./dwm/dwm-firefox.diff
  #     ];
  #   };
  # };

  services.udev.packages = [
    pkgs.android-udev-rules
  ];

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.william = {
    isNormalUser = true;
    extraGroups = ["wheel" "adbusers" "networkManager" "audio" "plugdev" "vboxusers" "dialout"];
  };

  users.extraGroups.vboxusers.members = [ "user-with-access-to-virtualbox" ];

  security.sudo.wheelNeedsPassword = false;

  documentation.dev.enable = true;

  virtualisation.virtualbox.host.enable = true;

  services.wakeonlan.interfaces = [ { interface = "enp3s0"; method = "password"; password = "DE:AD:66:CA:FE:99"; } ];
  # services.wakeonlan.interfaces = [ { interface = "enp3s0"; method = "magicpacket"; } ];
   # systemd.services.pre-poweroff =
   #    { description = "Pre-PowerOff Actions";
   #      wantedBy = [ "poweroff.target" ];
   #      before = [ "poweroff.target" ];
   #      script =
   #        ''
   #          ${config.powerManagement.powerDownCommands}
   #        '';
   #      serviceConfig.Type = "oneshot";
   #    };
  services.cron = {
    enable = true;
    systemCronJobs = [
      "@reboot root ${pkgs.ethtool}/bin/ethtool -s enp3s0 wol g"
    ];
  };

  # packageOverrides = super : let self = super.pkgs; in {
  #   kwin-tiling = super.kwin-tiling // {
  #     version = "2.4";
  #   };
  # };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

  # packageOverrides = super: let self = super.pkgs; in {
  #   kwin-tiling = super.kwin-tiling.override {
  #     version = "2.4";
  #     src = fetchFromGitHub {
  #       owner = "kwin-scripts";
  #       repo = "kwin-tiling";
  #       rev = "v${version}";
  #       sha256 = "095slpvipy0zcmbn0l7mdnl9g74jaafkr2gqi09b0by5fkvnbh37";
  #     };
  #   };
  # };
}

