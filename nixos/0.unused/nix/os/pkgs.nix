{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    firefox
    plasma-browser-integration
    xdg-desktop-portal-kde
    libreoffice
    calibre
    audacity
    discord
    vlc
    unrar
    zip
    unzip
    p7zip
    kwin-tiling
    libressl
    pciutils
    glxinfo
    file
    subversion
    ncurses5
    signal-desktop

    # Archives (e.g., tar.gz and zip)
    ark

    kdeplasma-addons

    # Screenshots
    kdeApplications.spectacle

    # Printing and scanning
    kdeApplications.print-manager
    simple-scan

    # KDE apps
    kdeFrameworks.kconfig
    kdeFrameworks.kconfigwidgets
    konsole
    dolphin
    kdeApplications.dolphin-plugins
    nordic
    manpages
    xdo

    # video record screen (to gif...)
    peek
    kcalc
    xcape
    sddm-kcm

    okular
  ];
}
