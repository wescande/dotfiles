{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
    neovim
    kitty
    zsh
    neofetch
    xclip
    htop
    curl
    wget
    lsd
    bat
    ripgrep
    fd
    ethtool
    dtrx
    picocom
  ];

  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;
}
