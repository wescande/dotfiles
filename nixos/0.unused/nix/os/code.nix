{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    binutils
    nodejs
    # vscodium
    cmake
    gnumake
    gcc
    git
    gitAndTools.delta
    smartgithg
    wireshark
    python
    python3
    rustup
    ccls
    tk
  ];
}
