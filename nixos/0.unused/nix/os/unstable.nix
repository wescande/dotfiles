{ config, pkgs, ... }:

let
  unstable = import <unstable> {};
  extensions = with pkgs.vscode-extensions; [
    ms-vscode.cpptools
    bbenoist.Nix
  ] ++ unstable.pkgs.vscode-utils.extensionsFromVscodeMarketplace [
    {
      name = "code-spell-checker";
      publisher = "streetsidesoftware";
      sha256 = "231c37e1270ca6cedb8b52f04251e6dfa2396335b1f648219945c94bf6fa8045";
      version = "1.9.0";
    }
    {
      name = "logcat-color";
      publisher = "raidxu";
      sha256 = "b2089197a8873d5bb44bcefba958d3707571926724626e648fab3487c8a40781";
      version = "0.0.1";
    }
    {
      name = "gitlens";
      publisher = "eamodio";
      sha256 = "ab22b88129b348fb0412615a8988d23ac0aff0be5c64fe8d34996199fe35d701";
      version = "10.2.2";
    }

  ];
  vscodium-with-extensions = unstable.pkgs.vscode-with-extensions.override {
    vscode = pkgs.vscodium;
    vscodeExtensions = extensions;
  };
in {
  environment.systemPackages = [
    unstable.razergenie
    unstable.flutterPackages.beta
    unstable.flutter
    vscodium-with-extensions
  ];
}

