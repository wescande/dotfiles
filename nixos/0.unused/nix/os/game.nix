{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    playonlinux
    lutris
    steam
    steam-run
  ];

  # Needed for steam in a 32bit env
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  hardware.pulseaudio.support32Bit = true;
}
