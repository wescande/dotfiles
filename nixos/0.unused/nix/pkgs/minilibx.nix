# { stdenv, fetchurl, pkgconfig, libX11, libXext }:

# stdenv.mkDerivation rec {
# 	name = "minilibx-${version}";
# 	version = "0.42";

# 	src = fetchurl {
# 		url = "https://projects.intra.42.fr/uploads/document/document/7/sources.tgz";
# 		sha256 = "1lwa9n1jrp2r157bls8vq5xblqs2dx2nhj0yly7p2jzz7mwk3njw";
# 	};

# 	nativeBuildInputs = [ pkgconfig libX11 libXext ];

# 	patches = [ ./disable_xshm.patch ];

# 	configurePhase = ''
# 		echo "INC=. \''${shell pkg-config --cflags x11 xext}" > Makefile
# 		cat Makefile.mk | grep -v %%%% >> Makefile
# 		cat Makefile
# 	'';

# 	installPhase = ''
# 		mkdir -p $out/include
# 		mkdir -p $out/lib
# 		install -m 644 libmlx.a $out/lib
# 		install -m 644 mlx.h $out/include
# 	'';
# }

{ stdenv, fetchFromGitHub, pkgconfig, libX11, libXext }:

stdenv.mkDerivation rec {
  name = "minilibx-${version}";
  version = "0.42";

  src = fetchFromGitHub {
    owner= "42Paris";
    repo = "minilibx-linux";
    rev= "2120a379df774f47be595b2fa5233f203d4520cb";
    sha256= "1knshnn9ywpqxjq9sh39wa6fxbkqgd0mk4zpq66ac2s8p6lsnplk";
    fetchSubmodules= false;
  };

  nativeBuildInputs = [ pkgconfig libX11 libXext ];

  # patches = [ ./disable_xshm.patch ];

  configurePhase = ''
    echo "INC=. \''${shell pkg-config --cflags x11 xext}" > Makefile
    cat Makefile.mk | grep -v %%%% >> Makefile
    cat Makefile
  '';

  installPhase = ''
    mkdir -p $out/include
    mkdir -p $out/lib
    install -m 644 libmlx.a $out/lib
    install -m 644 mlx.h $out/include
  '';
}