{ pkgs }:

{ allowUnfree = true; 
  packageOverrides = pkgs: rec {
    minilibx = pkgs.callPackage ./minilibx.nix {};
  };
}
# { pkgs }

# let
#   androidenv = import ./androidenv {};

#   androidComposition = androidenv.composeAndroidPackages {
#     platformToolsVersion = "29.0.6";
#   };
# in
# { allowUnfree = true; 
#   packageOverrides = pkgs: rec {
#     minilibx = pkgs.callPackage ./minilibx.nix {};
#   };
#   android_sdk.accept_licence = true;
#   packageOverrides = pkgs: {
#       androidComposition.androidsdk
#   };
# }
