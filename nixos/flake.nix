{
  inputs = {
    # nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs_unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    nur.url = github:nix-community/NUR;
    # home-manager.url = "github:nix-community/home-manager/release-20.09";
    home-manager.url = "github:nix-community/home-manager";
  };

  outputs = { self, nixpkgs, nixpkgs_unstable, nur, home-manager }:
  let
  machine = hardware: nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ({ pkgs, config, ... }: {
          nixpkgs.overlays = [
            (final: prev: {
              unstable = import nixpkgs_unstable { config = { allowUnfree = true; }; system = final.system; };
            })
            nur.overlay
          ];
        })
        hardware
        home-manager.nixosModules.home-manager
        ./configuration.nix
      ];
    };
  in {
    nixosConfigurations.WilliamLaptop = machine ./hardware/asus_r752.nix;
    nixosConfigurations.vbox = machine ./hardware/vbox.nix;
    nixosConfigurations.Tower = machine ./hardware/tower.nix;
    nixosConfigurations.WorkTower = machine ./hardware/work_tower.nix;
  };
}
