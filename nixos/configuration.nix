{ config, pkgs, ... }:

{
  imports = [
    ./users/william.nix
  ];
  boot = {
    supportedFilesystems = [ "zfs" ];
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      grub = {
        devices = [ "nodev" ];
        efiSupport = true;
        enable = true;
        version = 2;
        gfxpayloadEfi = "keep";
      };
    };
  };

  users.defaultUserShell = pkgs.zsh;

  systemd.services.NetworkManager-wait-online.enable = false;

  time.timeZone = "Europe/Paris";
  security.sudo.wheelNeedsPassword = false;
  security.pam.services.william.enableKwallet = true;

  services.xserver = {
    enable = true;
    layout = "us";
    xkbOptions = "caps:none,compose:ralt";
    autoRepeatDelay = 200;
    autoRepeatInterval = 25;
    displayManager = {
      # autoLogin.enable = true;
      # autoLogin.user = "william";
      sddm = {
        enable = true;
        autoNumlock = true;
      };
      defaultSession = "plasma5+bspwm";
    };
    desktopManager.plasma5.enable = true;
    windowManager.bspwm = {
      enable = true;
      configFile = "${../config/bspwm/bspwmrc}";
      sxhkd.configFile = "${../config/sxhkd/sxhkdrc}";
    };
  };

  programs = {
    adb.enable = true;
  };

  sound.enable = true;
  hardware.opengl.enable = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };
  hardware.openrazer.enable = true;
  nixpkgs.config.allowUnfree = true;

  fonts = {
    # enableFontDir = true;
    fontDir.enable = true;
    fonts = with pkgs; [
      (nerdfonts.override { fonts =  [ "FiraCode" "SourceCodePro" ]; })
    ];
  };
  services.flatpak.enable = true;
  nix = {
    package = pkgs.unstable.nixFlakes;

    extraOptions =
    # Add flakes
    ''
      experimental-features = nix-command flakes
    '' +
    # Add direnv
    ''
      keep-outputs = true
      keep-derivations = true
    '';
  };
}
