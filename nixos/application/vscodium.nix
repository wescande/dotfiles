pkgs:

pkgs.vscode-with-extensions.override {
    vscode = pkgs.vscodium;
    vscodeExtensions = with pkgs.vscode-extensions; [
      bbenoist.Nix
      ms-vscode.cpptools
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
      {
        name = "code-spell-checker";
        publisher = "streetsidesoftware";
        version = "1.10.2";
        sha256 = "1ll046rf5dyc7294nbxqk5ya56g2bzqnmxyciqpz2w5x7j75rjib";
      }
      {
        name = "logcat-color";
        publisher = "RaidXu";
        version = "0.0.1";
        sha256 = "1087lk48fd5bixj6wqi4cy972xbhsdcakyyf9fs5ngc7m2br225j";
      }
      {
        name = "gitlens";
        publisher = "eamodio";
        version = "11.1.3";
        sha256 = "1x9bkf9mb56l84n36g3jmp3hyfbyi8vkm2d4wbabavgq6gg618l6";
      }
      # {
      #   name = "cpptools";
      #   publisher = "ms-vscode";
      #   version = "1.1.3";
      #   sha256 = "02kx7yv3n22cja8pxnj0i03c4jx4v55iykcgbwxh37vknv8zv376";
      # }
    ];
  }
