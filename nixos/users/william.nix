{ config, lib, pkgs, ... }:

let
  homeConfig = ../../config;
  myvscodium = (import ../application/vscodium.nix) pkgs.unstable;
in {
  users.users.william = {
    isNormalUser = true;
    extraGroups = [ "wheel" "adbusers" "networkmanager" "audio" "plugdev" "dialout" ];
  };

  services.udev.packages = with pkgs.unstable; [ stlink ];
  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", GROUP="wheel", MODE="0666"
    SUBSYSTEM=="usb", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", GROUP="wheel", MODE="0666"
  '';

  home-manager.useGlobalPkgs = true;
  home-manager.useUserPackages = true;
  home-manager.users.william = {

    services.lorri = {
      enable = true;
      package = pkgs.lorri;
    };

    programs.direnv = {
      enable = true;
      enableZshIntegration = true;
    };

    programs.zsh = {
      enable = true;
    };
    programs.firefox = {
      enable = true;
      # extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      #   ublock-origin
      #   bitwarden
      # ];
      profiles.Default = {
        settings = {
          "browser.ctrlTab.recentlyUsedOrder" = false;
          "browser.tabs.warnOnClose" = false;
          "browser.startup.page" = 3;
          "intl.locale.requested" = "en-US,fr";
        };
      };
    };

    home.file = lib.listToAttrs (map (name:
      (lib.nameValuePair ".config/${name}" ({
        source = "${homeConfig}/${name}";
      }))) (builtins.attrNames (builtins.readDir homeConfig))) //
     { ".zshenv".source = "${homeConfig}/zsh/zshenv"; };
    home.packages = with pkgs.unstable;
      ([
        update-nix-fetchgit
        ## Terminal
        vim fzf
        (neovim.override {
          viAlias = true;
          vimAlias = true;
          configure = {
            plug.plugins = [];
            customRC = ''
              if filereadable($HOME . "/.config/nvim/init.vim")
                source $HOME/.config/nvim/init.vim
              endif
              '';
          };
        })
        neofetch
        xclip htop curl wget
        lsd     # Replace ls
        bat     # Replace cat
        ripgrep # Replace grep
        fd      # Replace find
        dtrx    # Awesome unArchive tool
        unrar zip unzip p7zip
        # ethtool

        ## Code
        binutils
        nodejs
        python python3
        rustup ccls
        cmake gnumake gcc git
        gitAndTools.delta # Use for git diff
        picocom
      ] ++ pkgs.lib.optionals config.services.xserver.enable [

        ## Code
        kitty
        arduino
	myvscodium
        # smartgithg # git GUI
        wireshark

        ## Game
        playonlinux
        lutris
        steam
        steam-run

        ## Others :
        plasma-browser-integration
        xdg-desktop-portal-kde
        libreoffice
        calibre
        audacity
        discord
        vlc
        pciutils
        glxinfo
        file
        subversion # svn

        # Printing and scanning
        libsForQt5.print-manager
        simple-scan

        # KDE apps
        ark # Archives (KDE)
        kdeplasma-addons
        libsForQt5.spectacle # Screenshots
        libsForQt5.kconfig
        libsForQt5.kconfigwidgets
        konsole # Terminal
        dolphin # File explorer
        libsForQt5.dolphin-plugins
        nordic
        manpages
        xdo

        peek # video record screen (to gif...)
        kcalc
        xcape # keyboard key substitute
        sddm-kcm

        okular # pdf viewer

        razergenie # Razer configuration tool
      ]);


  };
}

