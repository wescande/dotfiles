{ config, lib, pkgs, ... }:

# This creates a new 'nvidia-offload' program that runs the application passed to it on the GPU
# As per https://nixos.wiki/wiki/Nvidia
let
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    exec -a "$0" "$@"
  '';
in
{
  fileSystems = {
    "/"     = { fsType = "zfs";  device = "rpool/root/nixos"; };
    "/home" = { fsType = "zfs";  device = "rpool/home"; };
    "/boot" = { fsType = "vfat"; device = "/dev/disk/by-uuid/8B8D-18B0"; };
  };

  boot = {
    initrd = {
      availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
    };
    kernelModules = [ "kvm-intel" ];
    loader.grub = {
      gfxmodeEfi = "1280x1024";
      useOSProber = true;
    };
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  networking = {
    hostId = "e5b8cc3f";
    networkmanager.enable = true;
    # interfaces = {
    #   enp2sof1.useDHCP = true;
    #   wlp3s0.useDHCP = true;
    # };
  };

  hardware.cpu.intel.updateMicrocode = true;
  hardware.bluetooth.enable = true;

  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  environment.systemPackages = [ nvidia-offload ];

  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];

  hardware.nvidia.prime = {
    # sync.enable = true;
    offload.enable = true;
    nvidiaBusId = "PCI:4:0:0";
    intelBusId = "PCI:0:2:0";
    # Hardware should specify the bus ID for intel/nvidia devices
  };
  services.xserver.videoDrivers = [ "nvidia" ];
}
