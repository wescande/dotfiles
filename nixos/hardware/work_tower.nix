{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  fileSystems = {
    "/"     = { fsType = "zfs";  device = "zroot/root"; };
    "/nix"  = { fsType = "zfs";  device = "zroot/root/nix"; };
    "/home" = { fsType = "zfs";  device = "zroot/root/home"; };
    "/boot" = { fsType = "vfat"; device = "/dev/disk/by-uuid/1329-62A5"; };
  };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/6c6605b9-ea18-4abf-bfa2-f67fc61d65c4"; }
    ];

  boot = {
    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
    # loader.grub = {
    #   gfxmodeEfi = "1280x1024";
    # };
  };

  nix.maxJobs = lib.mkDefault 12;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  # hardware.video.hidpi.enable = lib.mkDefault true;

  networking = {
    hostId = "b5cc45b7";
    # networkmanager.enable = true;
    useDHCP = true;
  };
  # services.xserver.videoDrivers = [ "nvidia" ];
  hardware.cpu.intel.updateMicrocode = true;
}

