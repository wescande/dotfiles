{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  fileSystems = {
    "/"     = { fsType = "zfs";  device = "rpool/root/nixos"; };
    "/home" = { fsType = "zfs";  device = "rpool/home"; };
    "/boot" = { fsType = "vfat"; device = "/dev/disk/by-uuid/9815-2110"; };
  };

  swapDevices = [ ];

  boot = {
    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "uas" "sd_mod" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
    # loader.grub = {
    #   gfxmodeEfi = "1280x1024";
    # };
  };

  nix.maxJobs = lib.mkDefault 12;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  # hardware.video.hidpi.enable = lib.mkDefault true;

  networking = {
    hostId = "331987bd";
    # networkmanager.enable = true;
    useDHCP = true;
  };
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.cpu.intel.updateMicrocode = true;
}

