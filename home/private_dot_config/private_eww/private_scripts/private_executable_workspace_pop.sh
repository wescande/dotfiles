#! /bin/bash


current_monitor=1

get_activeWorkspace() {
  hyprctl monitors -j | jq --argjson arg "$1" '.[] | select(.id == $arg).activeWorkspace.id'
}

is_monitor_focused() {
  hyprctl monitors -j | jq --argjson arg "$1" '.[] | select(.id == $arg).focused'
}

focus_status=$(is_monitor_focused $current_monitor)
active_workspace=$(get_activeWorkspace $current_monitor)

workspace_pop() {
  old_focus_status=$focus_status
  old_active_workspace=$active_workspace
  focus_status=$(is_monitor_focused "$1")
  active_workspace=$(get_activeWorkspace "$1")
  if [ "$focus_status" == "false" ] ||
    { [ "$focus_status" == "$old_focus_status" ] &&
    [ "$active_workspace" -eq "$old_active_workspace" ]; }
  then
    return
  fi
  echo "true"
  sleep 0.8
  echo "false"
}

socat -U - UNIX-CONNECT:"$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock" | while read -r line; do
workspace_pop $current_monitor
done
