#!/bin/sh

# multiplication of $1 and $2
_m() {
  echo "${1} ${2}" | LC_ALL=C awk '{printf "%.0f\n",$1*$2}'
}

# "DP-2|eDP-1|eDP1"
INTERNAL_SCREEN="eDP1"
XRANDR=$(xrandr)

echo "$(date +"%T.%3N"):output of xrandr during sddm setup:" >> /home/wescande/sddm_xrandr_log
echo "${XRANDR}" >> /home/wescande/sddm_xrandr_log
XRAND_CONNECTED=$(echo "${XRANDR}" | grep -w 'connected' | cut -d ' ' -f1 )
PRIMARY_SCREEN=$(echo "${XRAND_CONNECTED}" | grep -E ${INTERNAL_SCREEN})
SECOND_SCREEN=$( echo "${XRAND_CONNECTED}" | grep -v -E ${INTERNAL_SCREEN})

read -r PRIMARY_SCREEN_X PRIMARY_SCREEN_Y <<EOF
$(echo "${XRANDR}" | rg -w "${PRIMARY_SCREEN}" -A 1 | rg -o '[0-9]+x[0-9]+\s'  |  sed -r 's/([0-9]+)x([0-9]+).*$/\1 \2/')
EOF

if [ -n "${SECOND_SCREEN}" ]
then
  # PRIMARY_SCALE=0.9999
  PRIMARY_SCALE=1
  PRIMARY_SCREEN_X_SCALE=$( _m "${PRIMARY_SCREEN_X}" ${PRIMARY_SCALE} )
  PRIMARY_SCREEN_Y_SCALE=$( _m "${PRIMARY_SCREEN_Y}" ${PRIMARY_SCALE} )

  SECOND_SCALE=1.75
  # SECOND_SCALE=1

  read -r SECOND_SCREEN_X SECOND_SCREEN_Y << EOF
$(echo "${XRANDR}" | rg -w "${SECOND_SCREEN}" -A 1 | rg -o "[0-9]+x[0-9]+\s"  |  sed -r 's/([0-9]+)x([0-9]+).*$/\1 \2/')
EOF
  SECOND_SCREEN_X_SCALE=$( _m "${SECOND_SCREEN_X}" ${SECOND_SCALE} )
  SECOND_SCREEN_Y_SCALE=$( _m "${SECOND_SCREEN_Y}" ${SECOND_SCALE} )

  HEIGHT=$(( PRIMARY_SCREEN_Y_SCALE > SECOND_SCREEN_Y_SCALE ? PRIMARY_SCREEN_Y_SCALE : SECOND_SCREEN_Y_SCALE ))
  WIDTH=$(( PRIMARY_SCREEN_X_SCALE+SECOND_SCREEN_X_SCALE ))
  SCREEN_SIZE="${WIDTH}x${HEIGHT}"

  xrandr --dpi 283 --fb "${SCREEN_SIZE}" \
    --output "${PRIMARY_SCREEN}" --primary \
      --mode "${PRIMARY_SCREEN_X}x${PRIMARY_SCREEN_Y}" \
      --scale "${PRIMARY_SCALE}" \
      --pos 0x0 \
      --panning "${PRIMARY_SCREEN_X_SCALE}x${PRIMARY_SCREEN_Y_SCALE}+0+0" \
    --output "${SECOND_SCREEN}" --auto \
      --mode "${SECOND_SCREEN_X}x${SECOND_SCREEN_Y}" \
      --scale "${SECOND_SCALE}" \
      --pos "${PRIMARY_SCREEN_X_SCALE}x0" \
      --panning "${SECOND_SCREEN_X_SCALE}x${SECOND_SCREEN_Y_SCALE}+${PRIMARY_SCREEN_X_SCALE}+0"
  echo "dual screen setup" >> /home/wescande/sddm_xrandr_log

else

  PRIMARY_SCALE=1
  PRIMARY_SCREEN_X_SCALE=$( _m "${PRIMARY_SCREEN_X}" ${PRIMARY_SCALE} )
  PRIMARY_SCREEN_Y_SCALE=$( _m "${PRIMARY_SCREEN_Y}" ${PRIMARY_SCALE} )

  SCREEN_SIZE=${PRIMARY_SCREEN_X_SCALE}x${PRIMARY_SCREEN_Y_SCALE}

  xrandr --dpi 283 --fb "${SCREEN_SIZE}" \
    --output "${PRIMARY_SCREEN}" --primary \
      --mode "${PRIMARY_SCREEN_X}x${PRIMARY_SCREEN_Y}" \
      --scale "${PRIMARY_SCALE}" \
      --pos 0x0 \
      --panning "${PRIMARY_SCREEN_X_SCALE}x${PRIMARY_SCREEN_Y_SCALE}+0+0"
  echo "one screen setup" >> /home/wescande/sddm_xrandr_log
fi
