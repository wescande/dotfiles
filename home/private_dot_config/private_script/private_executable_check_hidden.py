#!/usr/bin/env python3
import os
import subprocess
import sys
import glob
import re
import zipfile

bt_pattern = re.compile('(^Landroid/bluetooth)|(^Lcom/android/bluetooth)', re.IGNORECASE)

framework_only = False

def printUsages(apis):
  apiCount = 0
  apis = dict(sorted(apis.items(), key=lambda item: item[1].name))
  for api in apis.values():
    apiCount += 1
    print('HIDDEN API USAGE: %s' % (api.name))
    api.usages = sorted(api.usages, key=lambda usage: usage.where) 
    for usage in api.usages:
      print('\t%s in %s' % (usage.where, usage.apk))
    print()

def printStats(apis):
  apiCount = 0
  usageCount = 0
  for api in apis.values():
    apiCount += 1
    for usage in api.usages:
      usageCount += 1
  print('%d APIs used at %d locations' % (apiCount, usageCount))

def parseApk(device_path, apis, just_bt, is_base):
  print('parsing %s' % device_path)
  apk = os.getenv('OUT') + device_path
  result = subprocess.run(['./art/tools/veridex/appcompat.sh', '--dex-file=' + apk], capture_output=True, text=True)
  parseUsages(result.stdout, apis, just_bt, device_path, is_base)

def parseUsages(stdout, apis, just_bt, apk, is_base):
  api = None;
  is_bt_api = False
  for line in stdout.split('\n'):
    if api is None:
      if line.startswith('#'):
        name = line.split(' ')[3]
        if is_base and bt_pattern.search(name):
          apis = all_apis
        elif is_base:
          apis = bt_apk_apis
        if just_bt and not bt_pattern.search(name):
          continue
        api = apis.get(name)
        if api is None:
          api = HiddenApi(name)
          apis[name] = api
          is_bt_api = bt_pattern.search(name)
    else:
      usage = line.strip()
      if len(usage) == 0:
        # Remove APIs with no applicable usages
        if len(api.usages) == 0:
          del apis[api.name]
        api = None
      else:
        # If both API and usage are in bluetooth, ignore it
        is_bt_usage = bt_pattern.search(usage)
        if (is_bt_usage and not is_bt_api) or (not is_bt_usage and is_bt_api):
          api.addUsage(Usage(usage, apk))

class HiddenApi:
  def __init__(self, name):
    self.name = name
    self.usages = set()

  def addUsage(self, usage):
    self.usages.add(usage)

class Usage:
  def __init__(self, where, apk):
    self.where = where
    self.apk = apk

def ingest(glob_pattern, apis, just_bt):
  paths = glob.glob(os.getenv('OUT') + glob_pattern)
  for path in paths:
    parseApk(path[len(os.getenv('OUT')):], apis, just_bt, False)

def unzipFramework(frameworks_path, jar_name):
  jar = os.getenv('OUT') + frameworks_path + jar_name
  jarfile = zipfile.ZipFile(jar, "r" )
  framework_dexs = []
  for info in jarfile.infolist():
    filename = info.filename
    if filename.endswith(".dex"):
      if os.path.exists(os.getenv('OUT') + frameworks_path + filename):
        os.remove(os.getenv('OUT') + frameworks_path + filename)
      jarfile.extract(filename, os.getenv('OUT') + frameworks_path)
      framework_dexs.append(frameworks_path + filename)
  return framework_dexs

all_apis = {}
bt_apk_apis = {}
framework_apis = {}


def check_incoming_apks():
  # All of these should be just_bt=True
  ingest('/system/priv-app/*/*.apk', all_apis, just_bt=True)
  ingest('/product/priv-app/*/*.apk', all_apis, just_bt=True)
  ingest('/system/app/*/*.apk', all_apis, just_bt=True)
  ingest('/product/app/*/*.apk', all_apis, just_bt=True)

def check_outgoing_bluetooth():
  # just_bt=False
  parseApk('/system/app/Bluetooth/Bluetooth.apk', bt_apk_apis, just_bt=False, is_base=False)

def check_framework():
  framework_dexs = unzipFramework('/system/framework/', 'framework.jar')
  for dex in framework_dexs:
    parseApk(dex, framework_apis, just_bt=False, is_base=True)

check_incoming_apks()
check_outgoing_bluetooth()
check_framework()

print()
print()
print('----------------------BT api usages:----------------------')
printUsages(all_apis)
print()
print()
print('----------------------Usages of APIs inside BT:----------------------')
printUsages(bt_apk_apis)
print()
print()

print('BT api usages:')
printStats(all_apis)
print()
print()
print('Usages of APIs inside BT:')
printStats(bt_apk_apis)
