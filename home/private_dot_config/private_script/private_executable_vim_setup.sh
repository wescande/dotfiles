#!/usr/bin/env sh

command -v nvim || ( echo -e "\033[31m\`nvim\` command not found. Please install nvim first\033[0m" && false ) &&
  nvim +PlugInstall +'CocInstall coc-rust-analyzer coc-sh coc-clangd' +qall
