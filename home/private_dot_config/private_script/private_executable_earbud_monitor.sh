#!/usr/bin/env bash

echo "Destroy st-util and gdb session"
killall -q picocom 2>&- >&-
[ $# -eq 1 ] && [ "$1" == "-k" ] && echo "Script end" && exit 0

# PORT1=4242
# PORT2=4243

# echo "Get setup info"
# STM32F4_BOARD=$(st-info --probe | grep serial | sed 's/^.*serial: //g')
# SERIAL_2_USB_BOARD=$(ls -l /dev/ttyUSB* | sed 's/^.*\/dev\/tty/\/dev\/tty/')
# STM32_COUNT=$(wc -l <<< $STM32F4_BOARD)
# if [ "x$STM32_COUNT" != "x2" ]
# then
# 	echo "There are [$STM32_COUNT] board connected. [2] are needed"
# 	exit 1
# fi
# SERIAL_COUNT=$(wc -l <<< $SERIAL_2_USB_BOARD)
# ENABLE_SERIAL=true
# if [ "x$SERIAL_COUNT" != "x2" ]
# then
# 	echo "There are [$SERIAL_COUNT] board connected. [2] are needed to start serial log prog"
# 	ENABLE_SERIAL=false
# 	SERIAL1="None"
# 	SERIAL2="None"
# else
# 	SERIAL1=$(head -n 1 <<< $SERIAL_2_USB_BOARD)
# 	SERIAL2=$(tail -n 1 <<< $SERIAL_2_USB_BOARD)
# fi

# BOARD1=$(head -n 1 <<< $STM32F4_BOARD)
# BOARD2=$(tail -n 1 <<< $STM32F4_BOARD)
# echo -e "First  board is [$BOARD1]{$SERIAL1}"
# echo -e "Second board is [$BOARD2]{$SERIAL2}"
# $TERMINAL -e picocom -b 115200 $SERIAL1 &
# $TERMINAL -e picocom -b 115200 $SERIAL2 &

CMD="layout tall\n"
for i in $(find /dev/ -name "ttyUSB*" | sort)
do
  # CMD=$(echo $CMD"launch picocom -q -b 115200 $i \n")
  CMD=$(echo $CMD"launch picocom -q -b 1000000 $i \n")
done

# echo -e $CMD
kitty --session <( echo -e $CMD ) &
