
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'tpope/vim-fugitive'"
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'airblade/vim-gitgutter'"
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'zivyangll/git-blame.vim'"

let g:gitgutter_map_keys = 0

let s:fugitive_regex = '^fugitive://.*\.git//'

function! s:gitStatusOrClose() abort
  if empty(matchstr(bufname(), s:fugitive_regex .'$'))
    G
    if !empty(matchstr(bufname(), s:fugitive_regex .'$'))
      exe 'resize '. string(&lines * 0.5)
    endif
  else
    q
  endif
endfunction

autocmd BufReadPost fugitive://* set bufhidden=delete

function! s:gitDiffOrClose()
  let name = bufname()
  if !empty(matchstr(name, s:fugitive_regex .'$'))
    " current buffer is status - do nothing
    return
  endif
  if !empty(matchstr(name, s:fugitive_regex .'.*$'))
    " current buffer is already fugitive
    bunload
    return
  endif
  let relative_name = fnamemodify(name, ':.')
  let regex = s:fugitive_regex .'.*'. name
  " let regex = 'fugitive://.*\.git//.*' . name
  let relative_regex = s:fugitive_regex .'.*'. relative_name
  " let relative_regex = 'fugitive.*\.git.*' . relative_name
  for buf_index in nvim_list_bufs()
    if buf_index == bufnr() || !nvim_buf_is_loaded(buf_index)
      continue
    endif
    if !empty(matchstr(nvim_buf_get_name(buf_index), regex)) || !empty(matchstr(nvim_buf_get_name(buf_index), relative_regex))
      " There is a relative fugitive buffer
      exe "bunload" buf_index
      return
    endif
  endfor
  Gvdiffsplit
endfunction

" nnoremap <silent>gdf :call <SID>gitDiffOrClose()<CR>
" nnoremap <silent>gdf :call <SID>gitDiffOrClose()<CR>
" nnoremap <silent>gst :call <SID>gitStatusOrClose()<CR>
nnoremap <silent><leader>g :call <SID>gitStatusOrClose()<CR>
nnoremap <silent><leader>d :call <SID>gitDiffOrClose()<CR>

nnoremap <Leader>b :<C-u>call gitblame#echo()<CR>
