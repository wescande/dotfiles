let mapleader = " "

inoremap jk <esc>

"Tabs navigation
nnoremap <C-t> :tabnew<CR>
nnoremap <Tab> :<C-U>tabnext<CR>
vnoremap <Tab> :<C-U>tabnext<CR>
nnoremap <S-Tab> :<C-U>tabprevious<CR>
vnoremap <S-Tab> :<C-U>tabprevious<CR>

"windows navigation
nnoremap <leader>h       :wincmd h<CR>
nnoremap <leader>j       :wincmd j<CR>
nnoremap <leader>k       :wincmd k<CR>
nnoremap <leader>l       :wincmd l<CR>
nnoremap <leader><left>  :wincmd h<CR>
nnoremap <leader><down>  :wincmd j<CR>
nnoremap <leader><up>    :wincmd k<CR>
nnoremap <leader><right> :wincmd l<CR>
nnoremap <silent><leader><space> :let @/ = ""<cr>

" Cursor is centered when searching
nnoremap n nzz
nnoremap N Nzz

" Do not jump line when it's wrap
nnoremap j      gj
nnoremap k      gk
nnoremap <down> gj
nnoremap <up>   gk

noremap <C-u>   <C-r>
inoremap <C-u>  <Esc><C-r>i

noremap + :vertical resize +1<CR>
noremap - :vertical resize -1<CR>

" Search for selected in visual mode
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>


nnoremap <A-Left> <C-o>
nnoremap <A-Right> <C-i>
