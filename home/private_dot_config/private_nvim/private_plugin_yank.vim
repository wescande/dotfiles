" let external_sourced_plugin = external_sourced_plugin . "\n Plug 'ojroques/vim-oscyank'"
" Send a string to the terminal's clipboard using OSC52.
function! OSCYankString(str)
  let jobid = jobstart('/usr/local/google/home/wescande/.config/script/yank')
  call chansend(jobid, a:str)
  call chanclose(jobid, 'stdin')
endfunction

nnoremap <silent> <expr> <Plug>OSCYank OSCYankOperator('')

command! -range OSCYank <line1>,<line2>call OSCYankVisual()
command! -nargs=1 OSCYankReg call OSCYankString(getreg(<f-args>))


autocmd TextYankPost * if v:event.operator is 'y' && v:event.regname is '' | execute 'OSCYankReg "' | endif
autocmd TextYankPost * if v:event.operator is 'd' && v:event.regname is '' | execute 'OSCYankReg "' | endif
