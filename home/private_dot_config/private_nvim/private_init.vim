let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set mouse=a

let indentation_length=2
execute "set tabstop=". indentation_length
execute "set shiftwidth=". indentation_length
execute "set softtabstop=". indentation_length
set expandtab
set cc=80
set cursorline
set smartcase
set ignorecase

set termguicolors

set nofixendofline

" use the same clipboard as system
set clipboard+=unnamedplus

"reload current file at buffer change
set autoread
au FocusGained,BufEnter * :checktime

" Enabled / Disabled placeholder chars
let display_placeholder=1
" Character placeholder for tabulation [2 char]
let tab_placeholder='»·'
" Character placeholder for space [1 char]
let space_placeholder='·'
if !empty(display_placeholder)
  execute "set list listchars=tab:". tab_placeholder .",trail:". space_placeholder
endif

let s:path = fnamemodify(resolve(expand('<sfile>:p')), ':h')
let external_sourced_plugin = ""
exec 'source' s:path . '/mapping.vim'
exec 'source' s:path . '/plugin_nerdtree.vim'
exec 'source' s:path . '/plugin_fzf.vim'
exec 'source' s:path . '/plugin_coc.vim'
exec 'source' s:path . '/plugin_git.vim'
exec 'source' s:path . '/plugin_statusline.vim'
" exec 'source' s:path . '/plugin_yank.vim'
" exec 'source' s:path . '/plugin_comment.vim'
" exec 'source' s:path . '/plugin_indent.vim'

"Plugins settings
call plug#begin(stdpath('data') . '/plugged')
Plug 'rubberduck203/aosp-vim'
Plug 'arcticicestudio/nord-vim' "theme color
Plug 'myusuf3/numbers.vim'
Plug 'bfrg/vim-cpp-modern'
Plug 'rust-lang/rust.vim'
Plug 'wellle/targets.vim'
Plug 'LnL7/vim-nix'
" Plug 'conormcd/matchindent.vim'
Plug 'nmac427/guess-indent.nvim'
Plug 'chriskempson/base16-vim'
Plug 'gburca/vim-logcat'
Plug 'tpope/vim-commentary'

Plug 'neovim/nvim-lspconfig'

Plug 'udalov/kotlin-vim'
Plug 'gbprod/cutlass.nvim'

Plug 'anuvyklack/pretty-fold.nvim'

" Plug 'brymer-meneses/grammar-guard.nvim'

" if exists('$TMUX')
" Plug 'ojroques/vim-oscyank'
" endif

"expand sourced plugin
exe external_sourced_plugin
call plug#end()

let g:enable_numbers = 1
let g:numbers_exclude = ['unite', 'tagbar', 'startify', 'gundo', 'vimshell', 'w3m', 'nerdtree', 'Mundo', 'MundoDiff', 'fzf', 'help', 'man' , '', 'fugitiveblame']

let g:rustfmt_autosave = 1
let g:coc_filetype_map = {'java': 'latex'}

if !empty($BASE16_THEME)
  if filereadable(expand("~/.vimrc_background"))
    let base16colorspace=256
    source ~/.vimrc_background
  endif
else
  colorscheme nord
endif


lua <<EOF

require("cutlass").setup({
  exclude = { "nd", "nD" },
--, "sd", "sD", "vd", "vD", "Vd", "VD", "d", "D" },
})

vim.api.nvim_set_keymap("v", "d", "d", { noremap = true })

-- Setup lspconfig
require('guess-indent').setup {
  auto_cmd = true,  -- Set to false to disable automatic execution
  filetype_exclude = {  -- A list of filetypes for which the auto command gets disabled
    "netrw",
    "tutor",
  },
  buftype_exclude = {  -- A list of buffer types for which the auto command gets disabled
    "help",
    "nofile",
    "terminal",
    "prompt",
  },
}

if 1 == vim.fn.executable("kotlin-language-server") then
  local nvim_lsp = require'lspconfig'
  require'lspconfig'.kotlin_language_server.setup{
    root_dir = nvim_lsp.util.root_pattern('.git', 'Android.bp');
  }
end

-- require("grammar-guard").init()

-- require("lspconfig").grammar_guard.setup({
-- settings = {
  -- ltex = {
    -- enabled = { "latex", "tex", "bib", "markdown", "java", "cpp", "rust", "gitcommit" },
    -- language = "en",
    -- diagnosticSeverity = "information",
    -- sentenceCacheSize = 2000,
    -- additionalRules = {
      -- enablePickyRules = true,
      -- motherTongue = "en",
      -- },
    -- trace = { server = "verbose" },
    -- dictionary = {},
    -- disabledRules = {},
    -- hiddenFalsePositives = {},
    -- },
  -- },
-- })



EOF


syntax on
autocmd BufEnter * syntax match cType "\<[t]_\w\+\>"
autocmd BufEnter * syntax match cType "\<\w\+_[t]\>"

autocmd Filetype java setlocal cc=100
autocmd FileType gitcommit setlocal cc=72
autocmd FileType rust setlocal cc=100
autocmd FileType kotlin setlocal cc=100
autocmd FileType cpp setlocal cc=100

augroup filetype
  au! BufRead,BufNewFile *.s    set filetype=asm
  au! BufRead,BufNewFile *.asm  set filetype=asm
  au! BufRead,BufNewFile *.toml set filetype=toml
  " au! BufRead,BufNewFile *.bp   set filetype=bp
  au! BufRead,BufNewFile *.hpp  set filetype=cpp
  au! BufRead,BufNewFile *.cpp  set filetype=cpp
  au! BufRead,BufNewFile *.cc   set filetype=cpp
  au! BufRead,BufNewFile *.pdl  set filetype=bp
  au! BufRead,BufNewFile *.proto  set filetype=proto
augroup END

setlocal commentstring=//\ %s
autocmd FileType asm setlocal commentstring=;\ %s
autocmd FileType c,h,cpp setlocal commentstring=//\ %s
autocmd FileType proto setlocal commentstring=//\ %s
autocmd FileType cpp setlocal commentstring=//\ %s
autocmd FileType toml setlocal commentstring=#\ %s
autocmd FileType bp setlocal commentstring=//\ %s

" Allow auto-Align of function argument on '('
set cinoptions=(0
" Same but for all filetype because java sucks
" autocmd FileType * set cino=(0

