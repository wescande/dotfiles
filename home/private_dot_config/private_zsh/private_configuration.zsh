
setopt AUTO_CD              # Auto changes to a directory without typing cd.
setopt MULTIOS              # Write to multiple descriptors.
setopt EXTENDED_GLOB        # Use extended globbing syntax.

export FZF_DEFAULT_COMMAND='fdfind --type f --hidden --follow --exclude .git'
export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --color fg:#D8DEE9,bg:#2E3440,hl:#A3BE8C,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C --color pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#81A1C1,marker:#EBCB8B"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# export USER="william"
export MAIL="wescande@google.com"
export EDITOR=nvim
export VISUAL=nvim

# compdef tmx2=tmux
bindkey "^E" end-of-line # Map end-of-line key in the same way as zprezto editor module to prevent issue with tmux-resurrect.

export PATH="${HOME}/.local/bin${PATH:+:${PATH}}"
export PATH="${HOME}/.local/bin/platform-tools${PATH:+:${PATH}}"

export _JAVA_AWT_WM_NONREPARENTING=1

# use bat for color syntaxing the man
command -v bat 1>/dev/null 2>/dev/null && export MANPAGER="sh -c 'col -bx | bat -l man -p'" MANROFFOPT="-c"

# -R for color escape
# -i for insensitive case
# (removed) -c to stick the less to the top of the shell → does not works with a lot of tools
# (unused) -F to not use pager if it can fit in one page
export BAT_PAGER="less -Ri"

# git override pager default options and this is needed to enforce not having X
export LESS="-RFi"
