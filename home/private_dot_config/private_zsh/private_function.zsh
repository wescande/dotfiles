

# Expands .... to ../..
function expand-dot-to-parent-directory-path {
  if [[ $LBUFFER = *.. ]]; then
    LBUFFER+='/..'
  else
    LBUFFER+='.'
  fi
}
zle -N expand-dot-to-parent-directory-path
bindkey -M "emacs" "." expand-dot-to-parent-directory-path
bindkey -M "viins" "." expand-dot-to-parent-directory-path

bindkey -M isearch . self-insert 2> /dev/null


function zsh-prompt-benchmark() {
  emulate -L zsh

  zmodload zsh/datetime
  autoload -Uz add-zsh-hook
  add-zsh-hook precmd _zsh_prompt_benchmark_precmd

  typeset -gF3 _benchmark_prompt_duration=${1:-2}
  typeset -gi  _benchmark_prompt_warmup_duration=${2:-8}
  typeset -g   _benchmark_prompt_on_done=${3:-}
  typeset -gi  _benchmark_prompt_sample_idx=0
  typeset -gF3 _benchmark_prompt_start_time=0

  >&2 echo "Enabling prompt benchmarking for ${_benchmark_prompt_duration}s" \
           "after buffering keyboard input for ${_benchmark_prompt_warmup_duration}s."
  >&2 echo "Press and hold [ENTER] until you see benchmark results."

  function _zsh_prompt_benchmark_precmd() {
    local -F now=EPOCHREALTIME
    ((++_benchmark_prompt_sample_idx))
    ((now >= _benchmark_prompt_start_time + _benchmark_prompt_duration)) || return
    (( _benchmark_prompt_start_time )) && {
      emulate -L zsh
      local REPLY on_done=$_benchmark_prompt_on_done
      local -i n=$((_benchmark_prompt_sample_idx - 1))
      local -F3 t=$((now - _benchmark_prompt_start_time))
      local -F2 p=$((1000 * t / n))
      >&2 print -lr --                                                         \
        "********************************************************************" \
        "                      Prompt Benchmark Results                      " \
        "********************************************************************" \
        "Warmup duration      ${_benchmark_prompt_warmup_duration}s"           \
        "Benchmark duration   ${t}s"                                           \
        "Benchmarked prompts  $n"                                              \
        "Time per prompt      ${p}ms  <-- prompt latency (lower is better)"    \
        "********************************************************************" \
        ""                                                                     \
        "Press 'q' to continue..."
      unset -m "_benchmark_prompt_*"
      add-zsh-hook -D precmd _zsh_prompt_benchmark_precmd
      unset -f _zsh_prompt_benchmark_precmd
      IFS='' read -rsd q
      eval $on_done || true
    } || {
      sleep $_benchmark_prompt_warmup_duration
      typeset -gF _benchmark_prompt_start_time=EPOCHREALTIME
    }
  }
}

function startbase16() {
  BASE16_SHELL="${HOME}/.config/base16-shell/"
  [ -n "${PS1}" ] && \
    [ -s "${BASE16_SHELL}/profile_helper.sh" ] && \
    source "${BASE16_SHELL}/profile_helper.sh"
}

work() { tmx2 -f "${HOME}/.config/tmux/config" new-session -A -s ${1:-work}; }

catlog() { rogcat -i $1 --color=always -T '..*' | bat --style=plain }
savelog() { rogcat -o $1 --overwrite }
csavelog() { rogcat clear && savelog $@ }

function stopbase16() {
  unset BASE16_THEME
  kitty @ set-colors ${HOME}/.config/kitty/kitty.conf
}

# Add alls script from ~/.config/script/ into a function shell
for SCRIPT in $(find ${HOME}/.config/script -type f)
do
  NAME=$(<<<"${SCRIPT}" sed 's:^.*/\([^\.]*\)\(\..*\)\?$:\1:' )
  eval "${NAME}() { \"${SCRIPT}\" \$@ }"
done
