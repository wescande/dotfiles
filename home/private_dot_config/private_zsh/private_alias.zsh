
alias ls='lsd --group-dirs=first --date="+%d/%m/%y %T"'
alias l='ls -l'
alias ll='ls -l'
alias la='l -a'

alias vim='nvim'
alias ivm='nvim'
alias v='nvim'
alias vimdiff='nvim -d'
alias vd='nvim -d'
alias rvim='sudo -E nvim'

alias gst='git status'
alias gdf='git diff -w -b --ignore-space-at-eol --ignore-cr-at-eol'
alias gdfs='DELTA_FEATURES=+side-by-side gdf'
alias glol='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias glola='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit --all'
# alias glolb='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit --branches $(git branch -r | head -n 1)~...'
alias glolb='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit --branches $(git branch -r | rg -w main$ | head -n 1)~...'

alias gcam='git commit --all --message'
# alias gri='git rebase -i $(git branch -r | head -n 1)'
alias gri='git rebase -i $(git branch -r | rg -w main$ | head -n 1)'
alias grc='git rebase --continue'
alias gp='git push'
alias gcf='git commit --amend --no-edit'
alias gco='git checkout'
alias gcm='git commit --message'
alias gf='git fetch'
alias gfa='git fetch --all'
alias gcher='git cherry-pick'

alias rr='repo rebase .'
alias ru='repo upload . --cbr --ignore-hooks'

alias cat='bat'

alias fd='fdfind'

alias df='df -kh'
alias du='du -kh'
# alias open='xdg-open'

# alias c='f(){ codium $@; exit; }; f'

alias reporeset='repo forall -j99 -c git checkout --detach'

alias repo_topic="/google/bin/releases/android-keystone/repo_download_topic/repo_download_topic.par --topic"
alias ktfmt="/google/bin/releases/kotlin-google-eng/ktfmt/ktfmt --google-style"

alias checkApexSet='if [ -z ${APEX_TO_BUILD+x} ]; then echo "APEX_TO_BUILD is not set, setting it to aosp by default" ; aosp; fi'
alias checkOutSet='if [ -z ${OUT+x} ]; then echo "Need to source & lunch first."; false; fi'

alias sp="source build/envsetup.sh && lunch aosp_arm64-trunk_staging-eng"
alias sw="source build/envsetup.sh && lunch poplar-trunk_staging-eng"
alias sc="source build/envsetup.sh && lunch aosp_x86_64-trunk_staging-eng"
alias aosp="export APEX_TO_BUILD=com.android.btservices"
alias google="export APEX_TO_BUILD=com.google.android.btservices"
alias build_btservices='checkOutSet && checkApexSet && m $APEX_TO_BUILD'
alias install_btservices='checkOutSet && checkApexSet && adb install --force-non-staged $OUT/system/apex/$APEX_TO_BUILD.apex && adb shell su root killall system_server'

alias sapex='sc && aosp && echo "Apex set to \033[1;36mAOSP\033[0m on \033[1;35mCuttlefish\033[0m devices"'
alias sgapex='sc && google && echo "Apex set to \033[1;36mGoogle\033[0m on \033[1;35mCuttlefish\033[0m devices"'
alias bapex='build_btservices && echo "\n You just build an apex for \033[1;36m$APEX_TO_BUILD\033[0m and \033[1;35m$TARGET_PRODUCT\033[0m"'
alias iapex='install_btservices'

alias acid='/google/bin/releases/mobile-devx-platform/acid/acid'

alias autoco='rw wescande.c.googlers.com -t --tmux_session work --check_remaining --reconnect_loop'
