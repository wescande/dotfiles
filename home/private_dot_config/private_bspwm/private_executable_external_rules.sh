#!/bin/bash

function getName {
  xprop -id "$1" WM_NAME | cut -d\" -f2
}
function getRole {
  xprop -id "$1" WM_WINDOW_ROLE | cut -d\" -f2
}

wid=$1
class=$2
instance_name=$3
bspwm_win_config=$4

role=$(getRole "${wid}")
name=$(getName "${wid}")

case $class in
  Steam)
    if [ "$name" != "Steam" ]
    then
      echo border=off state=floating
    elif [[ "$bspwm_win_config" = *"state=floating"* ]]
    then
      echo border=off
    fi
    ;;
  Google-chrome)
    case $name in
      *"Google Meet"*);;
      *"Google Chat"*);;
      *"mail.google.com_/"*) ;;
      *"meet.google.com_/"*) ;;
      *)
        case $role in
          "pop-up") echo state=floating;;
        esac
        ;;
    esac
    ;;
  Firefox*)
    case $name in
      "Picture-in-Picture") echo border=off sticky=on state=floating;;
    esac
    ;;
  plasmashell)
    winType=$(xprop -id "$wid" _NET_WM_WINDOW_TYPE | awk -F "= " '{print $2}')
    if [[ "$winType" =~ .*"_NET_WM_WINDOW_TYPE_NOTIFICATION".* ]] ;
    then
      read -r elem_x elem_y <<<$(xprop -id "$1" | grep 'user specified size' | sed -E 's/^[^0-9]*([0-9]+) by ([0-9]+)/\1 \2/')
      read -r screen_x screen_y <<<$(xrandr | grep primary | cut -d ' ' -f4 | sed -r 's/([0-9]+)x([0-9]+).*$/\1 \2/')
      let pos_x=$screen_x/2-$elem_x/2 pos_y=$screen_y/2-$elem_y/2+$screen_y/3
      echo sticky=on state=floating border=off manage=on focus=off layer=above rectangle=${elem_x}x${elem_y}+${pos_x}+${pos_y}
    fi
    ;;
  *)
    case $name in
      "Live Caption") echo border=off state=floating focus=off;;
    esac
    ;;
esac
