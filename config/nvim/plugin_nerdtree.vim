
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'scrooloose/nerdtree'"
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'ryanoasis/vim-devicons'"

" Toggle Tree and return to previous windows
noremap <C-g> :NERDTreeToggle<CR>
"
" Close NerdTree if it's the last open
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()
function! s:CloseIfOnlyNerdTreeLeft()
	if exists("t:NERDTreeBufName")
		if bufwinnr(t:NERDTreeBufName) != -1
			if winnr("$") == 1
				q
			endif
		endif
	endif
endfunction

" calls NERDTreeFind if NERDTree is active, current window contains a modifiable file
function! s:syncTreeIf()
	if (exists("t:NERDTreeBufName") && bufwinnr(t:NERDTreeBufName) == 1 && bufname() != "" && empty(matchstr(bufname(), "NERD_tree*")))
		NERDTreeFind
		wincmd p
	endif
endfunction

" Smart synchronisation with opened file when switching between opened buffer
autocmd BufEnter * call s:syncTreeIf()
