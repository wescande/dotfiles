
" let external_sourced_plugin = external_sourced_plugin . "\n Plug 'tpope/vim-commentary'"

" nmap <C-_> gcc

" augroup filetype
" 	au! BufRead,BufNewFile *.s    set filetype=asm
" 	au! BufRead,BufNewFile *.asm  set filetype=asm
" 	au! BufRead,BufNewFile *.toml set filetype=toml
" 	au! BufRead,BufNewFile *.bp   set filetype=bp
" 	au! BufRead,BufNewFile *.hpp  set filetype=cpp
" 	au! BufRead,BufNewFile *.cpp  set filetype=cpp
" 	au! BufRead,BufNewFile *.cc   set filetype=cpp
" augroup END

" setlocal commentstring=//\ %s
" autocmd FileType asm setlocal commentstring=;\ %s
" autocmd FileType c,h,cpp setlocal commentstring=//\ %s
" autocmd FileType toml setlocal commentstring=#\ %s
" autocmd FileType bp setlocal commentstring=//\ %s
