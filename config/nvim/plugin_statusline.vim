
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'itchyny/lightline.vim'"
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'daviesjamie/vim-base16-lightline'"

if !empty($BASE16_THEME)
  let color='base16'
else
  let color='nord'
endif

let g:lightline = {
  \   'colorscheme': color,
  \   'active': {
  \     'left':[ [ 'mode', 'paste' ],
  \              [ 'cocstatus', 'gitbranch', 'readonly', 'filename', 'modified' ]
  \     ]
  \   },
	\   'component': {
	\     'lineinfo': ' %3l:%-2v',
	\   },
  \   'component_function': {
  \     'gitbranch': 'fugitive#head',
  \     'cocstatus': 'coc#status',
  \   }
  \ }
let g:lightline.separator = {
	\   'left': '', 'right': ''
  \}
let g:lightline.subseparator = {
	\   'left': '', 'right': '' 
  \}
let g:lightline.tabline = {
  \   'left': [ ['tabs'] ],
  \   'right': [ ['close'] ]
  \ }
"set showtabline=2  " Show tabline
"set guioptions-=e  " Don't use GUI tabline

augroup filetype_nerdtree
    au!
    au FileType nerdtree call s:disable_lightline_on_nerdtree()
    au WinEnter,BufWinEnter,TabEnter * call s:disable_lightline_on_nerdtree()
augroup END

fu s:disable_lightline_on_nerdtree() abort
    let nerdtree_winnr = index(map(range(1, winnr('$')), {_,v -> getbufvar(winbufnr(v), '&ft')}), 'nerdtree') + 1
    call timer_start(0, {-> nerdtree_winnr && setwinvar(nerdtree_winnr, '&stl', '%#Normal#')})
endfu
