
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'junegunn/fzf'"
let external_sourced_plugin = external_sourced_plugin . "\n Plug 'junegunn/fzf.vim'"

nnoremap ; :Buffers<CR>
nnoremap <C-p> :Files<CR>
" nnoremap <C-t> :Tags<CR>
nnoremap <C-f> :Rg <CR>
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)

" Reverse the layout to make the FZF list top-down
let $FZF_DEFAULT_OPTS='--layout=reverse'

" Using the custom window creation function
let g:fzf_layout = { 'window': 'call FloatingFZF()' }

" Function to create the custom floating window
function! FloatingFZF()
	" creates a scratch, unlisted, new, empty, unnamed buffer
	" to be used in the floating window
	let buf = nvim_create_buf(v:false, v:true)
	" 40% of the height
	let height = float2nr(&lines * 0.4)
	" 60% of the width
	let width = float2nr(&columns * 0.6)
	" horizontal position (centralized)
	let horizontal = float2nr((&columns - width) / 2)
	" vertical position (x line down of the top)
	let vertical = 3
	let opts = {
				\ 'relative': 'editor',
				\ 'style': 'minimal',
				\ 'row': vertical,
				\ 'col': horizontal,
				\ 'width': width,
				\ 'height': height
				\ }
	" open the new window, floating, and enter to it
	call nvim_open_win(buf, v:true, opts)
endfunction

let g:fzf_colors = {
			\ 'fg':      ['fg', 'Normal'],
			\ 'bg':      ['bg', 'Normal'],
			\ 'hl':      ['fg', 'Comment'],
			\ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
			\ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
			\ 'hl+':     ['fg', 'Statement'],
			\ 'info':    ['fg', 'PreProc'],
			\ 'border':  ['fg', 'Ignore'],
			\ 'prompt':  ['fg', 'Conditional'],
			\ 'pointer': ['fg', 'Exception'],
			\ 'marker':  ['fg', 'Keyword'],
			\ 'spinner': ['fg', 'Label'],
			\ 'header':  ['fg', 'Comment']
			\ }
function! s:findOrClose()
	if getbufvar(winbufnr('quickfix'), '&buftype') == 'quickfix'
		cclose
	else
		Rg
	endif
endfunction

nnoremap <silent><C-f> :call <SID>findOrClose()<CR>

