
alias ls='lsd --group-dirs=first --date="+%d/%m/%y %T"'
alias l='ls -l'
alias ll='ls -l'
alias lg='l --git'
alias la='l -a'

alias vim='nvim'
alias vimdiff='nvim -d'
alias ivm='nvim'
alias v='nvim'
alias vd='nvim -d'
alias rvim='sudo -E nvim'

alias gst='git status'
alias gdf='git diff'
alias glol='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias glola='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit --all'
alias gcam='git commit --all --message'
alias gp='git push'
alias gcf='git commit --amend --reuse-message HEAD'
alias gco='git checkout'
alias gcm='git commit --message'
alias gf='git fetch'
alias gfa='git fetch --all'
alias gcher='git cherry-pick'

alias cat='bat'
# alias grep='rg'

alias df='df -kh'
alias du='du -kh'
# alias open='xdg-open'

alias c='f(){ codium $@; exit; }; f'

alias reporeset='repo forall -j99 -c git checkout --detach'
alias reporez="repo forall packages/apps/Bluetooth packages/apps/Settings system/bt frameworks/base -j99 -c 'echo -e \"Enter in \033[33m\$REPO_PROJECT\033[0m:\"; git checkout --detach ; git branch | grep -v detached | xargs git branch -D ; git restore . ; git clean -fd '"
alias repost="repo forall packages/apps/Bluetooth packages/apps/Settings system/bt frameworks/base -j99 -c 'echo -e \"Enter in \033[33m\$REPO_PROJECT\033[0m:\"; git status'"

alias repo_topic="/google/bin/releases/android-keystone/repo_download_topic/repo_download_topic.par --topic"

PATH="$HOME/.local/bin":$PATH
