# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

ZINIT_PATH="$HOME/.config/zinit"
declare -A ZINIT
ZINIT[BIN_DIR]="$ZINIT_PATH/bin"
ZINIT[HOME_DIR]="$ZINIT_PATH"
ZPFX="$ZINIT_PATH/polaris"

### Added by Zinit's installer
if [[ ! -f $ZINIT_PATH/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing DHARMA Initiative Plugin Manager (zdharma/zinit)…%f"
    command mkdir -p "$ZINIT_PATH" && command chmod g-rwX "$ZINIT_PATH"
    command git clone https://github.com/zdharma/zinit "$ZINIT_PATH/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f" || \
        print -P "%F{160}▓▒░ The clone has failed.%f"
fi
source "$ZINIT_PATH/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit installer's chunk

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}' # Completion half-non-sensitive
zstyle ':prezto:module:editor' dot-expansion 'yes'

# zinit light zinit-zsh/z-a-bin-gem-node
# Needed for fzf package
zinit light zinit-zsh/z-a-patch-dl

# zinit ice svn
zinit snippet PZT::modules/history

# zinit ice svn
zinit snippet PZT::modules/editor

# zinit ice wait"1" lucid as"program" pick"bin/git-dsf"
# zinit light zdharma/zsh-diff-so-fancy

zinit ice wait lucid blockf atpull'zinit creinstall -q .'
zinit light zsh-users/zsh-completions

zinit ice wait lucid \
  atload'
    unset HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_{FOUND,NOT_FOUND};
    bindkey -M emacs "$key_info[Up]" history-substring-search-up;
    bindkey -M emacs "$key_info[Down]" history-substring-search-down;
  '
zinit light zsh-users/zsh-history-substring-search

zinit ice wait lucid atinit"zpcompinit; zpcdreplay"
zinit light zdharma/fast-syntax-highlighting

set ZSH_AUTOSUGGEST_USE_ASYNC=1
zinit ice wait lucid atload"_zsh_autosuggest_start"
zinit light zsh-users/zsh-autosuggestions

zinit ice atclone"dircolors -b src/dir_colors > c.zsh" \
    atpull'%atclone' pick"c.zsh" nocompile'!' \
    atload'zstyle ":completion:*" list-colors “${(s.:.)LS_COLORS}”'
zinit light arcticicestudio/nord-dircolors

if [ -n "${commands[fzf-share]}" ]; then
  zinit ice multisrc"{key-bindings,completion}.zsh"
  zinit light "$(fzf-share)"
else
  zinit pack"binary+keys" for fzf
fi

# load custom files
zinit ice multisrc"{p10k,configuration,function,alias}.zsh"
zinit light "$HOME/.config/zsh"

zinit ice depth=1; zinit light romkatv/powerlevel10k

# zinit from"gh-r" as"program" mv"direnv* -> direnv" \
#     atclone'./direnv hook zsh > zhook.zsh' atpull'%atclone' \
#     pick"direnv" src="zhook.zsh" for \
#         direnv/direnv
