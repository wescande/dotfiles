#!/usr/bin/env sh

check_command()
{
  command -v $1 > /dev/null ||
    (printf "\033[33m\`$1\` command not found.\033[0m\n" 1>&2 && false)
}

check_command nvim &&
  nvim +PlugInstall +'CocInstall coc-rust-analyzer coc-sh coc-ccls' +qall
check_command bat
check_command rg
check_command lsd
check_command delta
check_command htop

BASE16_SHELL="$HOME/.config/base16-shell/"
[ -d "${BASE16_SHELL}" ] || ( printf "\033[32m Installing base16-shell to ${BASE16_SHELL}\033[0m\n"; git clone https://github.com/chriskempson/base16-shell.git ${BASE16_SHELL})

