#! /bin/sh

function getName {
  xprop -id "$1" WM_NAME | cut -d\" -f2
}

wid=$1
class=$2
# echo "ARGS: $@" >> ~/TEST
# echo "pass here ARGS: $@" >> ~/TEST_2

case $class in
  Steam)
    name=$(getName ${wid})
    case $name in
      "Friends List") echo border=off state=floating;;
      "Steam - News"*) echo border=off state=floating;;
    esac
    ;;
  Firefox)
    name=$(getName ${wid})
    case $name in
      "Picture-in-Picture") echo border=off sticky=on state=floating;;
    esac
    ;;
  plasmashell)
    winType=$(xprop -id $wid _NET_WM_WINDOW_TYPE | awk -F "= " '{print $2}')
    if [ "$winType" = "_KDE_NET_WM_WINDOW_TYPE_ON_SCREEN_DISPLAY, _NET_WM_WINDOW_TYPE_NOTIFICATION" ] ;
    then
      echo sticky=on state=floating border=off manage=on focus=off layer=above rectangle=380x52+1090+934
    fi
    ;;
esac
